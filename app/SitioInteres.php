<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;

class SitioInteres extends Model
{
    //
    protected $fillable = ['id', 'nombre', 'descripcion','latitude','longitude'];
   // public function categoria(){
  //      return $this->belongsTo('App\Categoria');   }

  public function categorias(){

    return $this->belongsToMany('App\Categoria');
  

    //return $categoria = SitioInteres::find(2)->orderBy('nombre')->get();

  }
  
}
