<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estacion extends Model
{
    protected $fillable = ['nombre','latitude','longitude'];

    public function lineas(){
        return $this->belongsToMany(Linea::class);
    }
}
