<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;
use Phaza\LaravelPostgis\Geometries\Point;

class Sitio extends Model
{
    use PostgisTrait;

    protected $fillable = ['id', 'nombre', 'descripcionPrevia', 'descripcionFinal','disponibilidadHorariaSitio', 'latitudSitio', 'longitudSitio'];

    public function categorias()
    {
        return $this->belongsToMany('App\Categoria');
    }

    public function lineas()
    {
        return $this->belongsToMany('App\Linea');
    }
    
    //  protected $postgisFields = [
    //      'location'
    //  ];
}
