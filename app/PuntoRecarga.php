<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PuntoRecarga extends Model
{
    protected $fillable = ['nombre', 'descripcion','latitude','longitude'];
}
