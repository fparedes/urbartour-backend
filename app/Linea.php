<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Linea extends Model
{
    protected $fillable = ['nombre'];

     public function estaciones(){
         return $this->belongsToMany(Estacion::class);
     }
}
