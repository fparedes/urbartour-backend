<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SitioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'descripcionPrevia' => $this->descripcionPrevia,
            'descripcionFinal' => $this->descripcionFinal,
            'disponibilidadHoraria' => $this->disponibilidadHoraria,
            'latitud' => $this->latitud,
            'longitud' => $this->longitud

        ];
    }
}
