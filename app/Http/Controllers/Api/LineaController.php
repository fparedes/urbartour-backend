<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Linea;
use App\Http\Resources\LineaResource;


class LineaController extends Controller
{
    public function index()
    {
      //  $lineas = Linea::orderBy('id','DESC')->paginate(20);

        //return LineaResource::collection($lineas);
        return json_encode(Linea::orderBy('id','DESC')->get());
    }

    public function show(Linea $linea)
    {
        return new LineaResource($linea);
    }


    public function store(Request $request)
    {
        $linea = $this->validate($request, [
            'nombre'=>'required',
            'estacion_id'=>'required'
        ]);

        echo json_encode($linea);

        $linea = Linea::create($linea);

        return new LineaResource($linea);
    }

        public function update(Request $request, $id)    {
            
            $linea = Linea::find($id);
    
            $this->validate($request,[ 
                'nombre'=>'required',
                'estacion_id'=>'required'
            ]);
       
            //Aca se va a actualizar la linea.  
            $linea->nombre = $request->input('nombre');
            $linea->estacion_id = $request->input('estacion_id');
            $linea->save();
    }

    public function destroy(Request $request,$id){
        $linea = Linea::find($id);
        $linea->delete();
    }
}
