<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PuntoRecarga;
use App\Http\Resources\PuntoRecargaResource;

class PuntoRecargaController extends Controller
{
    public function index()
    {
        //$puntosRecarga = PuntoRecarga::orderBy('id','DESC')  ->paginate(20);

       // return PuntoRecargaResource::collection($puntosRecarga);
       return json_encode(PuntoRecarga::orderBy('id','DESC')->get());
    }

    public function show(PuntoRecarga $puntoRecarga)
    {
        return new PuntoRecargaResource($puntoRecarga);
    }


    public function store(Request $request)
    {
        $puntoRecarga = $this->validate($request, [
            'nombre'=>'required', 
            'descripcion'=>'required',
            'latitude' =>'required',
            'longitude' =>'required'
            ]);

        echo json_encode($puntoRecarga);

        $puntoRecarga = PuntoRecarga::create($puntoRecarga);

        return new PuntoRecargaResource($puntoRecarga);
    }

        public function update(Request $request, $id)    {
            
            $puntoRecarga = PuntoRecarga::find($id);
        
            $this->validate($request,[ 
        	'nombre'=>'required', 
        	'descripcion'=>'required'
            ]);

            $puntoRecarga->nombre = $request->input('nombre');
            $puntoRecarga->descripcion = $request->input('descripcion');
            $puntoRecarga->save();
    }

    public function destroy(Request $request,$id){
        $puntoRecarga = PuntoRecarga::find($id);
        $puntoRecarga->delete();
    }
}
