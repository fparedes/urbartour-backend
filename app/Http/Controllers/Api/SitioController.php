<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;    
use App\Http\Resources\SitioResource;
use App\Sitio;

class SitioController extends Controller
{
    public function index()
    {
    
       return Sitio::all(); 
    }

    public function show(Sitio $sitio)
    {
        return new SitioResource($sitio);
    }

    public function store(Request $request)
    {
        $sitio = $this->validate($request, [
            'nombre'=>'required', 
            'descripcionPrevia'=>'required', 
            'descripcionFinal'=>'required',
            'disponibilidadHoraria'=>'required',
            'latitud'=>'required',
            'longitud'=>'required'
        ]);

        echo json_encode($sitio);
        $sitio = Sitio::create($sitio);
        return new SitioResource($sitio);
    }
        //--------- FUNCION PARA MODIFICAR UN SITIO--------//
        public function update(Request $request, $id) 
           {
        $sitio = Sitio::find($id);

        $this->validate($request,[ 
            'nombre'=>'required', 
            'descripcionPrevia'=>'required', 
            'descripcionFinal'=>'required',
            'disponibilidadHoraria'=>'required',
            'latitud'=>'required',
            'longitud'=>'required'
        ]);

        $sitio->nombre = $request->input('nombre');
        $sitio->descripcionPrevia = $request->input('descripcionPrevia');
        $sitio->descripcionFinal = $request-input('descripcionFinal');
        $sitio->disponibilidadHoraria = $request-input('disponibilidadHoraria');
        $sitio->disponibilidadHoraria = $request-input('disponibilidadHoraria');
        $sitio->disponibilidadHoraria = $request-input('latitud');
        $sitio->disponibilidadHoraria = $request-input('longitud');
        $sitio->save();

        //$sitio = Sitio::find($id);
        //$sitio->update($request->all());
        //return new SitioResource($sitio);
    }

    //------ FUNCION PARA ELIMINAR UN SITIO----------//
    public function destroy(Request $request,$id){
        $sitio = Sitio::find($id);
        $sitio->delete();
    }

    //--- FUNCION PARA LISTAR LOS SITIOS DE LA CAT PASADA ---//
    public function findCategoria(Request $request, $id){

        $sitio = Sitio::find($id)->categorias;
        return $sitio;
        // return $prueba = $si->categorias();
    }

      //--- FUNCION PARA LISTAR LOS SITIOS DE LA CAT PASADA ---//
    //   public function findLinea(Request $request, $id){

    //     $sitio = Sitio::find($id)->lineas;
    //     return $sitio;
    // }
}
