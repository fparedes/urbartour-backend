<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categoria;
use App\Http\Resources\CategoriaResource;

class CategoriaController extends Controller
{
    //
    public function index()
    {
        //$categorias = Categoria::orderBy('id','DESC')
        //  ->paginate(20);
        //         $output = new \Symfony\Component\Console\Output\ConsoleOutput();
        //         $output->writeln("<info>Error message</info>");
        // return '[{"id": 3, "nombre": "Escultura"}, {"id": 2, "nombre": "Comedor"}, {"id": 1, "nombre": " Mural "}]';
        //return json_encode($categorias);
        //return CategoriaResource::collection($categorias);
        
        return json_encode(Categoria::orderBy('id','DESC')->get());
    }

    public function show(Categoria $categoria)
    {
        return new CategoriaResource($categoria);
    }


    public function store(Request $request)
    {
        $categoria = $this->validate($request, [
            'nombre'=>'required'
        ]);

        $categoria = Categoria::create($categoria);

        return new CategoriaResource($categoria);
    }

        public function update(Request $request, $id)    {
            
            $categoria = Categoria::find($id);
    
            $this->validate($request,[ 
                'nombre'=>'required'
            ]);
       
            //Aca se va a actualizar la categoria.
            
            $categoria->nombre = $request->input('nombre');
            $categoria->save();

    }

    public function destroy(Request $request,$id){
        $categoria = Categoria::find($id);
        $categoria->delete();
    }
}
