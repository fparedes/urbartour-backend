<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SitioInteres;
use App\Http\Resources\SitioInteresResource;
use Log;

class SitioInteresController extends Controller
{
    public function index()
    {
        //$sitiosInteres = SitioInteres::orderBy('id','DESC')->paginate(20);

        //return SitioInteresResource::collection($sitiosInteres);

        return json_encode(SitioInteres::orderBy('id','DESC')->get());
    }
    

    public function findCategoria(){
        return SitioInteres::find(2)->categorias;
       // return $prueba = $si->categorias();
    }


    public function show(SitioInteres $sitioInteres)
    {
        return new SitioInteresResource($sitioInteres);
    }


    public function store(Request $request)
    {
        $sitioInteres = $this->validate($request, [
            'nombre'=>'required', 
        	'descripcion'=>'required', 
            //'categoria_id'=>'required',
            'latitude'=>'required',
            'longitude'=>'required'
            ]);

        echo json_encode($sitioInteres);

        $sitioInteres = SitioInteres::create($sitioInteres);

        return new SitioInteresResource($sitioInteres);
    }

        public function update(Request $request, $id)    {  
            
            $sitioInteres = SitioInteres::find($id);
        
            $this->validate($request,[ 
        	'nombre'=>'required', 
            'descripcion'=>'required', 
            'latitude'=>'required',
            'longitude'=>'required'
        	//'categoria_id'=>'required'
            ]);

            $sitioInteres->nombre = $request->input('nombre');
            $sitioInteres->descripcion = $request->input('descripcion');
            $sitioInteres->latitude = $request-input('latitude');
            $sitioInteres->longitude = $request-input('longitude');
            //$sitioInteres->categoria_id = $request->input('categoria_id');
            $sitioInteres->save();
    }

    public function destroy(Request $request,$id){
        $sitioInteres = SitioInteres::find($id);
        $sitioInteres->delete();
    }
}
