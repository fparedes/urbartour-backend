<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Estacion;
use App\Http\Resources\EstacionResource;

class EstacionController extends Controller
{
    public function index()
    {
        //$estaciones = Estacion::orderBy('id','DESC')->paginate(20);

        //return EstacionResource::collection($estaciones);

        return json_encode(Estacion::orderBy('id','DESC')->get());
    }

    public function show(Estacion $estacion)
    {
        return new EstacionResource($estacion);
    }


    public function store(Request $request)
    {
        $estacion = $this->validate($request, [
            'nombre'=>'required',
            'latitude' =>'required',
            'longitude' =>'required'
        ]);

        $estacion = Estacion::create($estacion);

        return new EstacionResource($estacion);
    }

        public function update(Request $request, $id)    {
            $estacion = Estacion::find($id);
    
            $this->validate($request,[ 
                'nombre'=>'required',
                'latitude' =>'required',
                'longitude' =>'required'
            ]);
       
            //Aca se va a actualizar la estacion.
            
            $estacion->nombre = $request->input('nombre');
            $estacion->latitude = $request->input('latitude');
            $estacion->longitude = $request->input('longitude');
            $estacion->save();
    }

    public function destroy(Request $request,$id){
        $estacion = Estacion::find($id);
        $estacion->delete();
    }
}
