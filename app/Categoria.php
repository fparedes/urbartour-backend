<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{

    //
    public $timestamp=false;
    protected $filliable = ['id', 'nombre'];
}
