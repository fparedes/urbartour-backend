<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLineaSitioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linea_sitio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('linea_id')->unsigned();
            $table->integer('sitio_id')->unsigned();
            $table->timestamps();


            $table->foreign('linea_id')->references('id')->on('lineas');
            $table->foreign('sitio_id')->references('id')->on('sitios');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linea_sitio');
    }
}
