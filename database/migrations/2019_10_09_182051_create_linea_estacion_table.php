<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLineaEstacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linea_estacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('linea_id')->unsigned();
            $table->integer('estacion_id')->unsigned();
            $table->timestamps();

            $table->foreign('linea_id')->references('id')->on('lineas');
            $table->foreign('estacion_id')->references('id')->on('estacions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linea_estacion');
    }
}
