<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SitioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sitios')
        ->insert([
            'nombre' => 'Monumento al Indio',
            'descripcionPrevia' => 'La piedra fundamental de este monumento realizado',
            'descripcionFinal' => 'La piedra fundamental de este monumento realizado por el escultor argentino Luis Perlotti (1890/1969) en el año 1965, fue colocada en el centenario de la llegada de los galeses a estas tierras.',
            'disponibilidadHoraria' => 'las 24hs disponible',
<<<<<<< Updated upstream
            'latitud' => -42.7808718,
            'longitud' => -65.0035657,
=======
            'latitud' => -42.780282,
            'longitud' => -64.999230,
>>>>>>> Stashed changes
            'imagen' => 'FOTO INDIO',
            'favorito' => 0


        ]);  
        DB::table('sitios')
        ->insert([
            'nombre' => 'Monumento a la mujer Galesa',
            'descripcionPrevia' => 'Fue encargado para conmemorar el centenario del desembarco',
            'descripcionFinal' => 'Fue encargado para conmemorar el centenario del desembarco en la provincia de Chubut del primer contingente de esa inmigración e inaugurado el 28 de julio de 1965 en Puerto Madryn.',
            'disponibilidadHoraria' => 'las 24hs disponible',
<<<<<<< Updated upstream
            'latitud' => -42.7680108,
            'longitud' => -65.0319634,
=======
            'latitud' => -42.766488,
            'longitud' => -65.032613,
>>>>>>> Stashed changes
            'imagen' => 'FOTO GALESA',
            'favorito' => 0
        ]);
 
        DB::table('sitios')
        ->insert([
            'nombre' => 'Museo del desembarco',
            'descripcionPrevia' => 'Se localiza en las cercanías del sitio en donde desembarcaron del Velero Mimosa',
            'descripcionFinal' => 'Se localiza en las cercanías del sitio en donde desembarcaron del Velero Mimosa 153 colonos galeses el 28 de julio de 1865 y donde fue el primer asentamiento de Puerto Madryn, durante los días posteriores.',
            'disponibilidadHoraria' => 'de 09:00 am a 20:00 pm ',
<<<<<<< Updated upstream
            'latitud' => -42.7808718,
            'longitud' => -65.0035657,
=======
            'latitud' => -42.780907,
            'longitud' => -65.001376,
>>>>>>> Stashed changes
            'imagen' => 'FOTO MUSEO DESEMBARCO',
            'favorito' => 0
        ]);

<<<<<<< Updated upstream
        DB::table('sitios')
        ->insert([
            'nombre' => 'UNPSJB',
            'descripcionPrevia' => 'Universidad Nacional de la Patagonia San Juan Bosco',
            'descripcionFinal' => 'La Universidad Nacional de la Patagonia San Juan Bosco, con sus siglas abreviadas «UNP» o de modo completo «UNPSJB», es una universidad pública argentina con sede central en la ciudad de Comodoro Rivadavia, provincia de Chubut. También posee sedes en Esquel, Trelew y Puerto Madryn, en la misma provincia.',
            'disponibilidadHoraria' => 'de 08:00 am a 22:00 pm ',
            'latitud' => -42.7853444,
            'longitud' => -65.0054861,
            'imagen' => 'FOTO UNPSJB',
            'favorito' => 0
        ]);       
=======
         DB::table('sitios')
        ->insert([
            'nombre' => 'Museo oceanográfico',
            'descripcionPrevia' => 'El objetivo del Museo de Ciencias Naturales y Oceanográfico es preservar',
            'descripcionFinal' => 'El objetivo del Museo de Ciencias Naturales y Oceanográfico es preservar y difundir las riquezas del ecosistema marino de Puerto Madryn.',
            'disponibilidadHoraria' => '09:00 am',
            'latitud' => -42.762312,
            'longitud' => -65.039805,
            'imagen' => 'FOTO MUSEO OCEANOGRAFICO',
            'favorito' => 0
        ]);

        DB::table('sitios')
        ->insert([
            'nombre' => 'Mural El Regreso',
            'descripcionPrevia' => 'La obra de arte recrea el desembarco de los',
            'descripcionFinal' => 'La obra de arte recrea el desembarco de los veteranos de guerra en el puerto chubutense tras el conflicto armado en el Atlántico Sur.',
            'disponibilidadHoraria' => 'las 24hs disponible',
            'latitud' => -42.737109,
            'longitud' => -65.040620,
            'imagen' => 'IMAGEN MURAL EL REGRESO',
            'favorito' => 1
        ]);

        DB::table('sitios')
        ->insert([
            'nombre' => 'Comedor Catri',
            'descripcionPrevia' => 'Atrás quedaron aquellos tiempos en',
            'descripcionFinal' => 'Atrás quedaron aquellos tiempos en los que todos los niños y niñas se iban a comer a casa.',
            'disponibilidadHoraria' => '11:00 am 15:00 pm',
            'latitud' => -42.759002,
            'longitud' => -65.046554,
            'imagen' => 'MAGEN COMEDOR CATRI',
            'favorito' => 1
        ]);

        DB::table('sitios')
        ->insert([
            'nombre' => 'Teatro del muelle',
            'descripcionPrevia' => 'ACTIVIDADES  Talleres de Danza y Teatro',
            'descripcionFinal' => 'ACTIVIDADES  Talleres de Danza y Teatro / Música en vivo / Obras de Teatro / Charlas / Conferencias de Prensa.',
            'disponibilidadHoraria' => '08:00 am 19:30 pm',
            'latitud' => -42.762877,
            'longitud' => -65.035271,
            'imagen' => 'IMAGEN MUSEO DEL MUELLE',
            'favorito' => 1
        ]);
        DB::table('sitios')
        ->insert([
            'nombre' => 'Teatro La Escalera',
            'descripcionPrevia' => 'Primer Sala de Teatro Independiente de la ciudad de',
            'descripcionFinal' => 'Primer Sala de Teatro Independiente de la ciudad de Puerto Madryn. Usina de la teatralidad y pasion. Sala de espectáculos teatrales. Ámbito de creación, entrenamiento y formación. Caracteristicas: Capacidad para 60 personas espectadoras.',
            'disponibilidadHoraria' => '17:00 am 23:30 pm',
            'latitud' => -42.774736,
            'longitud' => -65.034171,
            'imagen' => 'IMAGEN LA ESCALERA',
            'favorito' => 1
        ]);
        DB::table('sitios')
        ->insert([
            'nombre' => 'Parada Mitre y 28 de Julio',
            'descripcionPrevia' => 'Parada del centro',
            'descripcionFinal' => 'Parada del centro',
            'disponibilidadHoraria' => '6:00 am 23:30 pm',
            'latitud' => -42.766601,
            'longitud' => -65.036176,
            'imagen' => 'IMAGEN PARADA 28 JULIO Y MITRE',
            'favorito' => 1
        ]);
        DB::table('sitios')
        ->insert([
            'nombre' => 'Parada Univerdidad',
            'descripcionPrevia' => 'Parada',
            'descripcionFinal' => 'Esta Parada le permite a los alumnos de la Univerdidad San Juan Bosco transportarse de manera directa',
            'disponibilidadHoraria' => '6:00 am 23:30 pm',
            'latitud' => -42.785026,
            'longitud' => -65.006360,
            'imagen' => 'IMAGEN PARADA UNI',
            'favorito' => 1
        ]);
        DB::table('sitios')
        ->insert([
            'nombre' => 'Recarga tarjeta Ceferino',
            'descripcionPrevia' => 'Maxi kiosco Mio',
            'descripcionFinal' => 'B° Conquistadores del desierto. Albarracin y Gob. Maíz',
            'disponibilidadHoraria' => '6:00 am 23:00 pm',
            'latitud' => -42.774190,
            'longitud' => -65.036278,
            'imagen' => 'IMAGEN KIOSCO MIO',
            'favorito' => 1
        ]);
        DB::table('sitios')
        ->insert([
            'nombre' => 'Recarga tarjeta Ceferino',
            'descripcionPrevia' => 'Kiosco Emoji',
            'descripcionFinal' => 'Centro. Mitre entre Belgrano y 28 de Julio',
            'disponibilidadHoraria' => '8:00 am 22:00 pm',
            'latitud' => -42.767373,
            'longitud' => -65.035691,
            'imagen' => 'IMAGEN KIOSCO EMOJI',
            'favorito' => 1
        ]);
>>>>>>> Stashed changes
    }
}
