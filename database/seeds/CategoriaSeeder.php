<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')
        ->insert([
            'nombre' => 'Monumento'
        ]);  
        DB::table('sitios')
        ->insert([
            'nombre' => 'Museo'
        ]);
         DB::table('sitios')
        ->insert([
            'nombre' => 'Mural'
        ]);
        DB::table('sitios')
        ->insert([
            'nombre' => 'Comedor'
        ]);
        DB::table('sitios')
        ->insert([
            'nombre' => 'Teatro'
        ]);
        DB::table('sitios')
        ->insert([
            'nombre' => 'Parada'
        ]);
        DB::table('sitios')
        ->insert([
            'nombre' => 'Punto recarga'
        ]);
    }
}
